function checkvalue(form) { 
    var productName = form.productName.value;
    var producer = form.producer.value;
    var model = form.model.value;
    var dateOfIssue = form.dateOfIssue.value;
    var color = form.color.value;
    var price = form.price.value;
    
    var isValid = true;
    
    var warnings = [];
    
    var productNameRegExp = "[a-zA-Z]+";
    var producerRegExp = "[a-zA-Zа-яА-Я]+";
    var modelRegExp = "[a-zA-Zа-яА-Я]{2}[0-9]{3}";
    var dateOfIssueRegExp = "(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-([1-2][0-9]{3})";
    var colorRegExp = "[a-zA-Z]+";
    var priceeRegExp = "(0.(0*[1-9]+|[1-9]+))|([1-9][0-9]*.[0-9]+)";
    
    //Form fields validation for null.
    if(!productName.match(productNameRegExp)) {
    	warnings.push("Name not correspond the pattern.");
        isValid = false;
    }
    if(!producer.match(producerRegExp)) {
    	warnings.push("Producer not correspond the pattern.");
        isValid = false;
    }
    if(!model.match(modelRegExp)) {
    	warnings.push("Model not correspond the pattern.");
        isValid = false;
    }
    if(!dateOfIssue.match(dateOfIssueRegExp)) {
    	warnings.push("Date not correspond the pattern.");
        isValid = false;
    }
    if(!color.match(colorRegExp)) {
    	warnings.push("Color not correspond the pattern.");
        isValid = false;
    }
    if(!price.match(priceeRegExp)) {
    	warnings.push("Price not correspond the pattern.");
        isValid = false;
    }
    
    //output of validation problems
    if(false == isValid){
    	alert(warnings.join("\n"));
    }  
    
    return isValid;
    
}
