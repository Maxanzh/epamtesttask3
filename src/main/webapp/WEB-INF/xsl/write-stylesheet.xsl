<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns="http://www.epam.com/catalogXMLSchema"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:nms="http://www.epam.com/catalogXMLSchema"
		>
    <xsl:output method="html" indent="yes"/>
   
	<xsl:output indent="yes"/>
	
	<xsl:param name="categoryName"/>
	<xsl:param name="subcategoryName"/>
	<xsl:param name="productName"/>
	<xsl:param name="producer"/>
	<xsl:param name="model"/>
	<xsl:param name="dateOfIssue"/>
	<xsl:param name="color"/>
	<xsl:param name="price"/>
    
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates
                select="node()|@*" />
        </xsl:copy>  
    </xsl:template>

    <xsl:template
        match="/nms:catalog/nms:category[@name=$categoryName]/nms:subcategory[@name=$subcategoryName]">
        <xsl:copy>
            <xsl:apply-templates
                select="node()|@*" />   
                  <xsl:element name="product">
                        <xsl:attribute name="name"><xsl:value-of select="$productName"/> </xsl:attribute>
                        <xsl:element name="producer"><xsl:value-of select="$producer"/> </xsl:element>
                        <xsl:element name="model"><xsl:value-of select="$model"/> </xsl:element>
                        <xsl:element name="dateOfIssue"><xsl:value-of select="$dateOfIssue"/> </xsl:element>
                        <xsl:element name="color"><xsl:value-of select="$color"/> </xsl:element>
                        <xsl:element name="price"><xsl:value-of select="$price"/> </xsl:element>
                    </xsl:element>                     
            
        </xsl:copy>
    </xsl:template> 	
    
</xsl:stylesheet>
