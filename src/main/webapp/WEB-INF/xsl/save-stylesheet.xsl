<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:nms="http://www.epam.com/catalogXMLSchema"
		xmlns:validator="xalan://com.epam.testapp.util.ProductValidator"
		exclude-result-prefixes="validator">

     <xsl:import href="write-stylesheet.xsl"/>
     <xsl:output method="html" indent="yes"/>

    <xsl:param name="xmlFilepath"/>
    <xsl:param name="categoryName"/>
    <xsl:param name="subcategoryName"/>
    <xsl:param name="productName"/>
    <xsl:param name="producer"/>
    <xsl:param name="model"/>
    <xsl:param name="dateOfIssue"/>
    <xsl:param name="color"/>
    <xsl:param name="price"/>
    <xsl:param name="validator"/>

    
    <xsl:template match="/">
       <xsl:if test="validator:validateProduct($validator,$productName, $producer, $model, $dateOfIssue, $color, $price)"/>
       <xsl:choose>
           <xsl:when test="false() = validator:isValidationFlag($validator)">
                <xsl:call-template name="validation"/>           
           </xsl:when>
           <xsl:otherwise>
                    <xsl:apply-imports />
           </xsl:otherwise>
       </xsl:choose>

    </xsl:template>

    <xsl:template name="validation">
        <html>
            <head>
                <title>Products</title>
                <link rel="stylesheet" type="text/css" href="css/main-style.css"></link>
                <script type="text/javascript" src="js/validation.js"></script>
            </head>
        <body>
        <table>
        <form action="controller" method="post">
            <input type="hidden" name="command" value="save"/>
            <input type="hidden" name="categoryName" value="{$categoryName}"/>
            <input type="hidden" name="subcategoryName" value="{$subcategoryName}"/>

            <tr>
                <td>Product Name: </td>
                <td><input type="text" name="productName" value="{$productName}"/></td>
                <td class="prompt"><xsl:value-of select="validator:getError($validator,'productName')"/></td>
            </tr>
            <tr>
                <td>Producer: </td>
                <td><input type="text" name="producer" value="{$producer}"/></td>
                <td class="prompt"><xsl:value-of select="validator:getError($validator,'producer')"/></td>
            </tr>
            <tr>
                <td>Model: </td>
                <td><input type="text" name="model" value="{$model}"/></td>
                <td class="prompt"><xsl:value-of select="validator:getError($validator,'model')"/></td>
            </tr>
            <tr>
                <td>Date of issue: </td>
                <td><input type="text" name="dateOfIssue" value="{$dateOfIssue}"/></td>
                <td class="prompt"><xsl:value-of select="validator:getError($validator,'dateOfIssue')"/></td>
            </tr>
            <tr>
                <td>Color: </td>
                <td><input type="text" name="color" value="{$color}"/></td>
                <td class="prompt"><xsl:value-of select="validator:getError($validator,'color')"/></td>
            </tr>
            <tr>
                <td>Price: </td>
                <td><input type="text" name="price" value="{$price}"/></td>
                <td class="prompt"><xsl:value-of select="validator:getError($validator,'price')"/></td>
            </tr>
            <tr>
                <td>
                    <input type="button" value="cancel" onclick="location.href='controller?command=back&amp;page_type=products&amp;categoryName={$categoryName}&amp;subcategoryName={$subcategoryName}'"/>
                </td>
                <td>
                    <input type="submit" value="save"></input>
                </td>
            </tr>
        </form>
        </table>
        </body>
        </html>
    </xsl:template>
                                     
    
</xsl:stylesheet>

        