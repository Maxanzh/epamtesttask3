<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:nms="http://www.epam.com/catalogXMLSchema">
	
	<xsl:output method="html" indent="yes"/>
	
	<xsl:param name="categoryName"/>
	<xsl:param name="subcategoryName"/>
    
	<xsl:template match="/">    
	<html>
    	<head>
            <link rel="stylesheet" type="text/css" href="css/main-style.css"/>
            <title>Products</title>
        </head>
    	<body>
    	 <div class="table-content">
    		<table class="products-style">
                    <tr>
                        <td colspan="6">Products of <xsl:value-of select="$subcategoryName"/> subcategory.</td>
                    </tr>
                    <tr>
                        <td>Product Name</td>
                        <td>Producer</td>
                        <td>Model</td>
                        <td>Date of issue</td>
                        <td>Color</td>
                        <td>Price</td>
                    </tr>
                    <xsl:for-each select="nms:catalog/nms:category/nms:subcategory[@name=$subcategoryName]/nms:product" >
                            <xsl:call-template name="takeTheProduct"></xsl:call-template>
                    </xsl:for-each>
    		</table>
    	 </div>
    	 <table class="products-backward-style">
            <tr>
                <td>
                    <input type="submit" value="back" onclick="location.href='controller?command=back&amp;page_type=subcategories&amp;categoryName={$categoryName}'"/>
                </td>
                <td>
                    <input type="submit" value="add" onclick="location.href='controller?command=to_add_page&amp;categoryName={$categoryName}&amp;subcategoryName={$subcategoryName}'"/>
                </td>
            </tr>
    	 </table>
		</body>
   	</html>
	</xsl:template>
	
	
	<xsl:template name="takeTheProduct">
            <xsl:variable name="currentProductName"><xsl:value-of select="@name"/></xsl:variable>

            <tr>
            <td><xsl:value-of select="$currentProductName"/></td>
            <td><xsl:value-of select="nms:producer"/></td>
            <td><xsl:value-of select="nms:model"/></td>
            <td><xsl:value-of select="nms:dateOfIssue"/></td>
            <td><xsl:value-of select="nms:color"/></td>
            <td>
                <xsl:choose>
                        <xsl:when test="nms:price">
                                <xsl:value-of select="nms:price"/>
                        </xsl:when>
                        <xsl:when test="nms:notInStock">
                                Not in stock
                        </xsl:when>
                </xsl:choose>
            </td>
            </tr>
	</xsl:template>
	
</xsl:stylesheet>