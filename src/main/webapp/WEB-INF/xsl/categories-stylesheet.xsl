<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:nms="http://www.epam.com/catalogXMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
	
	
<xsl:template match="/">
    <html>	
      <head>
          <link rel="stylesheet" type="text/css" href="css/main-style.css"/>
          <title>Categories</title>
      </head>
    <body>
            <div class="table-content">
            <table class="categories-style"> 
                    <tr>
                            <td>Categories</td>
                            <td>Count of products</td>
                    </tr>
               <xsl:for-each select="nms:catalog/nms:category">
                <tr>
                 <xsl:call-template name="category-link" />
                </tr>
               </xsl:for-each>
            </table>            
        </div>
    </body>
    </html>
</xsl:template>
	
    <xsl:template name="category-link">
        <xsl:variable name="categoryName"><xsl:value-of select="@name"/></xsl:variable>
        <td>
            <a href="controller?command=view_subcategories&amp;categoryName={$categoryName}">
                <xsl:value-of select="$categoryName"/>
            </a>
        </td>
        <td><xsl:value-of select="count(nms:subcategory/nms:product)" /></td>
 </xsl:template>

</xsl:stylesheet>

