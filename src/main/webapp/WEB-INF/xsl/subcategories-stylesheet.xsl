<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:nms="http://www.epam.com/catalogXMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
	
	<xsl:param name="categoryName"/>
		
	<xsl:template match="/">
	<html>
	
    	<head>
            <link rel="stylesheet" type="text/css" href="css/main-style.css"/>
            <title>Subcategories</title>
        </head>
    	<body>
    		<div class="table-content">
    			<table class="categories-style">
    			<tr>
                            <td>Subcategories:</td>
                            <td>Count of products</td>
    			</tr>
                            <xsl:for-each select="nms:catalog/nms:category[@name=$categoryName]/nms:subcategory" >
                                    <xsl:call-template name="takeTheProduct"></xsl:call-template>
                            </xsl:for-each>    
    			</table>
    		</div>
    		<table class="categories-backward-style">
    			<tr>
                            <td colspan="2">
                                <input type="submit" value="back" 
                                       onclick="location.href='controller?command=back&amp;page_type=categories'"/>
                            </td>
    			</tr>
    		</table>
		</body>
   	</html>
	</xsl:template>
	
	
	<xsl:template name="takeTheProduct">
		<xsl:variable name="subcategoryName"><xsl:value-of select="@name"/></xsl:variable>
		<tr>
                    <td>
                        <a href="controller?command=view_products&amp;categoryName={$categoryName}&amp;subcategoryName={$subcategoryName}">
                            <xsl:value-of select="$subcategoryName"/>
                        </a>
                    </td>
                    <td>
                        <xsl:value-of select="count(nms:product)"></xsl:value-of>
                        <br/>
                    </td>
		</tr>
	</xsl:template>
	
</xsl:stylesheet>