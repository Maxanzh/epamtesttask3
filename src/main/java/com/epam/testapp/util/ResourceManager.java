/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.util;

import java.util.ResourceBundle;

/**
 *
 * @author Maxim_Zhupinsky
 */
public class ResourceManager {
    
    private static String realPath;
      
    public static String getResource(String bundlePath, String resourcePath) {
        ResourceBundle bundle = ResourceBundle.getBundle(bundlePath);       
        return bundle.getString(resourcePath);         
    }
    
    public static String defineResourcePath(String bundlePath, String resourcePath) {
       ResourceBundle bundle = ResourceBundle.getBundle(bundlePath);   
       // System.out.println("real path "+realPath+bundle.getString(resourcePath));
       return realPath+bundle.getString(resourcePath);
    }
    
    public static String getRealPath() {
        return realPath;
    }

    public static void setRealPath(String realPath) {
        ResourceManager.realPath = realPath;
    }
}
