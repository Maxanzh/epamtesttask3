package com.epam.testapp.service;

public enum TransformationType {
    
    ADDITIONAL("appResource.filepath.xsl.additional"), 
    CATEGORIES("appResource.filepath.xsl.categories"), 
    SUBCATEGORIES("appResource.filepath.xsl.subcategories"),
    SAVE("appResource.filepath.xsl.save"),
    PRODUCTS("appResource.filepath.xsl.products"); 

    private TransformationType(String path) {
       bundlePath = path; 
    }
    // save path to bundle item with real path to xslt file
    private String bundlePath;

    public String defineBundlePath() {
        return bundlePath;
    }
        
}
