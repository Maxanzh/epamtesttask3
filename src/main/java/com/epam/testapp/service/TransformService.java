/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.service;

import com.epam.testapp.exception.LogicException;
import com.epam.testapp.util.ResourceManager;
import com.epam.testapp.util.AppConstants;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author Maxim_Zhupinsky
 */
public final class TransformService {
        
    private ReadWriteLock readWriteLocker = new ReentrantReadWriteLock();
    private TransformerFactory transformFactory = TransformerFactory.newInstance(); 
    private Map<TransformationType, Templates> templatesMap = new HashMap<>();
    private File xmlFile;
    private Source xmlSource;
    private long lastModified;
    
    private TransformService() {
    }
    
    public static TransformService getInstance() {
        return TransformationManagerHolder.INSTANCE;
    }
    
    private static class TransformationManagerHolder {

        private static final TransformService INSTANCE = new TransformService();
    }
    
    public Transformer defineXSLTTransformer(TransformationType type) throws LogicException {
        
        Transformer transformer;
        Templates chosenTemplate;

        try {    
            chosenTemplate = templatesMap.get(type);
            transformer = chosenTemplate.newTransformer();            
        } catch (TransformerConfigurationException ex) {
            throw new LogicException("can't create xslt transformer" , ex);
        }
        return transformer;
    }
    
    public void transformFile(Transformer transformer, 
            StreamResult stream) throws LogicException {
        
        try {
            readWriteLocker.readLock().lock();
            lastModified = xmlFile.lastModified();
            transformer.transform(xmlSource, stream); 
        } catch (TransformerException ex) {
            throw new LogicException("can't transform file "+xmlFile, ex);
        } finally {
            readWriteLocker.readLock().unlock();
        }
    }
    
    public boolean writeFile (String xmlFilePath, Transformer transformer,
            Writer transformResult, String categoryName, String subcategoryName) 
            throws LogicException {
        
        boolean result =false;
        try{
            readWriteLocker.writeLock().lock();
            // if file was re-write information should be update
            if (lastModified == xmlFile.lastModified()) {
                //Write to file
                Writer fileWriter = new PrintWriter(xmlFile, AppConstants.UTF);
                fileWriter.write(transformResult.toString());
                fileWriter.flush();
                result = true;
            }
        } catch (IOException ex) {
            throw new LogicException("can't transform file "+xmlFilePath, ex);
        } finally {
            readWriteLocker.writeLock().unlock();          
        }
        return result;
    }
        
    public void initialize() throws LogicException {
        
        String xslBundlePath;
        String xslFilePath;
        Source xsltSource;
        Templates template;
        
        try {
           String xmlPath = ResourceManager.defineResourcePath(AppConstants.RESOURCE_PATH,
                AppConstants.XML_FILE_PATH);
            xmlFile = new File(xmlPath);
            xmlSource = new StreamSource(xmlFile);
            
            for(TransformationType transformType: TransformationType.values()) {
                xslBundlePath = transformType.defineBundlePath();
                xslFilePath = ResourceManager.defineResourcePath(AppConstants.RESOURCE_PATH,
                        xslBundlePath);
                xsltSource = new StreamSource(xslFilePath);
                template = transformFactory.newTemplates(xsltSource);
//                System.out.println("type "+transformType+" templ "+template+"\n");
                templatesMap.put(transformType, template);
            }
        } catch (TransformerConfigurationException ex) {
            throw new LogicException("error initializing xsl template map", ex);        
        }
        }
}
  

