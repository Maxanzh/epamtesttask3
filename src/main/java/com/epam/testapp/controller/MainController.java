/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.controller;

import com.epam.testapp.command.ICommand;
import com.epam.testapp.command.factory.CommandFactory;
import com.epam.testapp.exception.LogicException;
import com.epam.testapp.service.TransformService;
import com.epam.testapp.util.ResourceManager;
import com.epam.testapp.util.AppConstants;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Maxim_Zhupinsky
 */
public class MainController extends HttpServlet {
    
    private final static Logger LOGGER = Logger.getLogger(MainController.class);     
    
   @Override
    public void init() throws ServletException {
        super.init();
        String realPath = this.getServletContext().getRealPath("/");
        ResourceManager.setRealPath(realPath);
        
        try {
            TransformService.getInstance().initialize();
        } catch (LogicException ex) {
            LOGGER.error(ex);
        }
    }         
            
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String strCommandName = request.getParameter(AppConstants.PARAM_COMMAND);
        ICommand command = CommandFactory.createCommand(strCommandName);
        
        try {
            LOGGER.info("Command "+command.getClass()+" action perfomed");
            command.execute(request, response);
        } catch (LogicException e) {
            e.printStackTrace();
            LOGGER.error(e);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


}
