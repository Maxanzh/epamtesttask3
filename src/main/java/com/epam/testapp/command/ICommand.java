/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.command;

import com.epam.testapp.exception.LogicException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maxim_Zhupinsky
 */
public interface ICommand {
    public void execute (HttpServletRequest request, HttpServletResponse response) throws LogicException;
}
