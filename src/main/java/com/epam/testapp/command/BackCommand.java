/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.command;

import com.epam.testapp.exception.LogicException;
import com.epam.testapp.service.TransformService;
import com.epam.testapp.service.TransformationType;
import com.epam.testapp.util.AppConstants;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;


public final class BackCommand implements ICommand {

    private enum BackPageTypeEnum{ CATEGORIES, SUBCATEGORIES, PRODUCTS	}
    private static TransformService transformService;
	
	
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws LogicException {
                
        StreamResult outputStream;
        Transformer transformer;
        String backActionParam = (String)request.getParameter(AppConstants.PARAM_PAGE_TYPE);
        
        if( null == backActionParam) {
            throw new LogicException("param back page is null");
        }
        
        try {
            transformService = TransformService.getInstance();
            outputStream = new StreamResult(response.getWriter());
            BackPageTypeEnum pageAction = BackPageTypeEnum.valueOf(backActionParam.toUpperCase());
            transformer = defineBackAction(pageAction, request);
            transformService.transformFile(transformer, outputStream); 
        } catch (IOException ex) {
            throw new LogicException(ex);
        }

    }
    
    private Transformer defineBackAction(BackPageTypeEnum pageKey, HttpServletRequest request)
            throws LogicException {
        
        Transformer transformer;
        String categoryName;
        String subcategoryName;
        
        switch( pageKey) {
            case CATEGORIES:
                transformer = transformService.defineXSLTTransformer(TransformationType.CATEGORIES);
                break;
            case SUBCATEGORIES:
                transformer = transformService.defineXSLTTransformer(TransformationType.SUBCATEGORIES);
                categoryName = request.getParameter(AppConstants.PARAM_CATEGORY_NAME);
                transformer.setParameter(AppConstants.PARAM_CATEGORY_NAME, categoryName);
                break;
            case PRODUCTS:
                transformer = transformService.defineXSLTTransformer(TransformationType.PRODUCTS);
                categoryName = request.getParameter(AppConstants.PARAM_CATEGORY_NAME);
                subcategoryName = request.getParameter(AppConstants.PARAM_SUBCATEGORY_NAME);
                transformer.setParameter(AppConstants.PARAM_CATEGORY_NAME, categoryName);
                transformer.setParameter(AppConstants.PARAM_SUBCATEGORY_NAME, subcategoryName);
                break;
            default:
                throw new LogicException("No such enum back action case");
        }
        return transformer;
    }
    
}


			