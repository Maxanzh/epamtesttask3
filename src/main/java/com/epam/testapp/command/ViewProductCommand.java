/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.command;

import com.epam.testapp.exception.LogicException;
import com.epam.testapp.service.TransformService;
import com.epam.testapp.service.TransformationType;
import com.epam.testapp.util.AppConstants;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;


public final class ViewProductCommand implements ICommand {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws LogicException {
        
        StreamResult outputStream;
        Transformer transformer;
        TransformService transformService;
        String categoryNameKey = request.getParameter(AppConstants.PARAM_CATEGORY_NAME);
        String subcategoryNameKey = request.getParameter(AppConstants.PARAM_SUBCATEGORY_NAME);
        
        if(categoryNameKey == null || subcategoryNameKey == null ) {
            throw new LogicException("param category or subcategory name is null");
        }
        
        try {
            
            outputStream = new StreamResult(response.getWriter());
            transformService = TransformService.getInstance();
            transformer = transformService.defineXSLTTransformer(TransformationType.PRODUCTS);
            transformer.setParameter(AppConstants.PARAM_CATEGORY_NAME, categoryNameKey);
	    transformer.setParameter(AppConstants.PARAM_SUBCATEGORY_NAME, subcategoryNameKey);
            transformService.transformFile(transformer, outputStream);  
            
        }  catch(IOException ex){
               throw new LogicException(ex);
        } 

    }
    
}
