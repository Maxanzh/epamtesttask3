package com.epam.testapp.command.factory;

public enum CommandEnum {
	TO_ADD_PAGE, VIEW_CATEGORIES, VIEW_SUBCATEGORIES, VIEW_PRODUCTS, SAVE, BACK

}
