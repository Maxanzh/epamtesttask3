package com.epam.testapp.command.factory;

import com.epam.testapp.command.BackCommand;
import com.epam.testapp.command.GoAddPageCommand;
import com.epam.testapp.command.ICommand;
import com.epam.testapp.command.SaveCommand;
import com.epam.testapp.command.ViewCategoriesCommand;
import com.epam.testapp.command.ViewProductCommand;
import com.epam.testapp.command.ViewSubcategoriesCommand;



public final class CommandFactory {

    public static ICommand createCommand(String commandName) {
        ICommand iCommand;
        CommandEnum enumCommandName;
        
        try {
	   enumCommandName = CommandEnum.valueOf(commandName.toUpperCase());
        } catch (Exception ex) {
        //    ex.printStackTrace();
           return new ViewCategoriesCommand();
        }
        
        switch(enumCommandName) {
            case VIEW_CATEGORIES:
                iCommand =  new ViewCategoriesCommand();
                break;
            
            case VIEW_SUBCATEGORIES:
                iCommand =  new ViewSubcategoriesCommand();
                break;
                
            case VIEW_PRODUCTS:
                iCommand =  new ViewProductCommand();
                break;    
                
            case TO_ADD_PAGE:
                iCommand =  new GoAddPageCommand();
                break;                   
            case BACK:
                iCommand =  new BackCommand();
                break;
            case SAVE:
                iCommand =  new SaveCommand();
                break;    
            default:
                iCommand =  new ViewCategoriesCommand();
                break;
        }
        return iCommand;
    }
	
}
