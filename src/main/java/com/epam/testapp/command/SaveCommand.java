/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.command;

import com.epam.testapp.exception.LogicException;
import com.epam.testapp.service.TransformService;
import com.epam.testapp.service.TransformationType;
import com.epam.testapp.util.AppConstants;
import com.epam.testapp.util.ProductValidator;
import com.epam.testapp.util.ResourceManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;


public final class SaveCommand implements ICommand {

    private String categoryName;
    private String subcategoryName;
    private String productName;
    private String producer;
    private String model;
    private String dateOfIssue;
    private String color;
    private String price;
    
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws LogicException {
        
        TransformService transformService = TransformService.getInstance();
        ProductValidator validator = new ProductValidator();
        
        String xmlPath = ResourceManager.defineResourcePath(AppConstants.RESOURCE_PATH,
                AppConstants.XML_FILE_PATH);

        // initialize class fields from request params
        defineRequestParam(request);
         
        Writer transformResult = new StringWriter();
        StreamResult outputStream = new StreamResult(transformResult);  

        Transformer transformer = transformService.defineXSLTTransformer(TransformationType.SAVE);               

        // check should information be upadate
        boolean isWriteFinished = false;
        
        while (!isWriteFinished) {
            setTransormerParams(transformer, xmlPath, validator);                
            transformService.transformFile(transformer, outputStream);

            try {
                if (validator.isValidationFlag()) { 
                    
                    isWriteFinished = transformService.writeFile(xmlPath, transformer,
                            transformResult,categoryName, subcategoryName);
                    //Redirect to product list
                    StringBuilder redirectBuilder = new StringBuilder(AppConstants.FORWARD_PRODUCT_LIST);
                    redirectBuilder.append(AppConstants.SUBCATEGORY_ID_PARAM);
                    redirectBuilder.append(subcategoryName);
                    redirectBuilder.append(AppConstants.CATEGORY_ID_PARAM);
                    redirectBuilder.append(categoryName);
                    response.sendRedirect(redirectBuilder.toString());

                } else {
                    PrintWriter writer = response.getWriter();
                    writer.write(transformResult.toString());
                    writer.flush();
                    isWriteFinished = true;
                }
            } catch (IOException ex) {
                throw new LogicException(ex);    
            }
        }
    }
    
    private void defineRequestParam(HttpServletRequest request) {
        categoryName = request.getParameter(AppConstants.PARAM_CATEGORY_NAME);
        subcategoryName = request.getParameter(AppConstants.PARAM_SUBCATEGORY_NAME);
        productName = request.getParameter(AppConstants.PARAM_PRODUCT_NAME);
        producer = request.getParameter(AppConstants.PARAM_PRODUCER);
        model = request.getParameter(AppConstants.PARAM_MODEL);
        dateOfIssue = request.getParameter(AppConstants.PARAM_DATE_OF_ISSUE);
        color = request.getParameter(AppConstants.PARAM_COLOR);
        price = request.getParameter(AppConstants.PARAM_PRICE);    
    }
    
    private void setTransormerParams(Transformer transformer, String xmlPath, 
            ProductValidator validator) {
        transformer.setParameter(AppConstants.XML_FILE_PATH, xmlPath);
        transformer.setParameter(AppConstants.PARAM_CATEGORY_NAME, categoryName);
        transformer.setParameter(AppConstants.PARAM_SUBCATEGORY_NAME, subcategoryName);
        transformer.setParameter(AppConstants.PARAM_PRODUCT_NAME, productName);
        transformer.setParameter(AppConstants.PARAM_PRODUCER, producer);
        transformer.setParameter(AppConstants.PARAM_MODEL, model);
        transformer.setParameter(AppConstants.PARAM_DATE_OF_ISSUE, dateOfIssue);
        transformer.setParameter(AppConstants.PARAM_COLOR, color);
        transformer.setParameter(AppConstants.PARAM_PRICE, price);   
        transformer.setParameter(AppConstants.PARAM_VALIDATOR, validator);   
    }
}
